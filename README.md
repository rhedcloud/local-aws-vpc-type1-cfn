# Pipeline to deploy vpc type 1 stack to local master accounts.

This pipeline is executed using the admin account - eventually account 0 in the member series - and should be setup
in the bitbucket pipeline variables accordingly.

## S3 Bucket targets
* Gather user id for target S3 bucket for cfn stack deployments.
	"aws s3api list-buckets" using the account that owns the S3
	bucket to find the owner ID. This is also available via the
	master account's target S3 bucket's Access Control List under
	the Permissions tab.
	
* Update the pipeline script accordingly

Test change. Ignore.